﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public static GameController Instance;
	public bool  isPlayerTurn;
	public bool areEnemiesMoving;
	public int playerCurrentHealth = 50;
	public AudioClip gameOverSound;
	public int charSelect;

	private BoardController boardController;
	private List<Enemy> enemies;
	private GameObject levelImage;
	private Text levelText;
	private GameObject startImage;
	private Text startText;
	private bool settingUpGame = false;
	private int secondsUntilLevelStart = 2;
	private int currentLevel = 1;
	
	void Awake () {
		if(Instance!= null && Instance!= this){
			DestroyImmediate(gameObject);
			return;
		}

		Instance = this;
		DontDestroyOnLoad(gameObject);
		boardController = GetComponent<BoardController>();
		enemies = new List<Enemy> ();
	}

	void Start(){
		startImage = GameObject.Find ("Start Image");
		startImage.SetActive (true);
		//StartScreen ();
	}

	/*private void StartScreen(){
		if (currentLevel <= 1) {
			startImage = GameObject.Find ("Start Image");
			startText = GameObject.Find ("Start Text").GetComponent<Text> ();
			startImage.SetActive (true);
			//Invoke ("DisableStartImage", secondsUntilLevelStart);
		} 
		else {
			InitializeGame ();
		}
	}*/

	public void chooseRogue(){
		charSelect = 0;
		DisableStartImage ();
	}

	public void chooseArcher(){
		charSelect = 1;
		DisableStartImage ();
	}

	private void DisableStartImage(){
		startImage.SetActive (false);
		InitializeGame ();
	}

	private void InitializeGame(){
			//settingUpGame = true;
		if (currentLevel > 1) {
			startImage = GameObject.Find ("Start Image");
			startImage.SetActive (false);
		}
			levelImage = GameObject.Find ("Level Image");
			levelText = GameObject.Find ("Level Text").GetComponent<Text> ();
			levelText.text = "Day " + currentLevel;
			levelImage.SetActive (true);
			enemies.Clear ();
			boardController.SetupLevel (currentLevel);
			//settingUpGame = false;
			isPlayerTurn = true;
			areEnemiesMoving = false;
			Invoke ("DisableLevelImage", secondsUntilLevelStart);
		
	}

	private void DisableLevelImage(){
		
		levelImage.SetActive (false);

	}

	private void OnLevelWasLoaded(int levelLoaded){
		currentLevel++;
		InitializeGame ();
	}

	void Update () {
		//if (isPlayerTurn || areEnemiesMoving || settingUpGame) {
			//Player.Instance.healthText.text = "isP? " + isPlayerTurn.ToString() + " e? " + areEnemiesMoving.ToString() + " s? " + settingUpGame.ToString();
			//return;
		//}
		//Player.Instance.healthText.text = "Enemy turn!";
		//StartCoroutine (MoveEnemies ());
	}
	private IEnumerator MoveEnemies(){
		areEnemiesMoving = true;

		yield return new WaitForSeconds (0.2f);

		foreach (Enemy enemy in enemies) {
			enemy.MoveEnemy();
			yield return new WaitForSeconds(enemy.moveTime);
		}

		areEnemiesMoving = false;
		isPlayerTurn = true;
	}

	public void AddEnemyToList(Enemy enemy){
		enemies.Add (enemy);
	}

	public void GameOver(){
		isPlayerTurn = false;
		SoundController.Instance.music.Stop ();
		SoundController.Instance.PlaySingle (gameOverSound);
		levelText.text = "You starved after " + currentLevel + " days...";
		levelImage.SetActive (true);
		enabled = false;
	}
}